Installation :
    - composer install
    - npm install

Lancer le projet :
    - symfony serve
    - npm run dev-server

Au lancement du site, s'ouvre la page d'acceuil avec en haut le menu.
Le bouton Home ramène sur la page d'acceuil.
Le bouton musique permet d'afficher les musiques. Il faut etre connecté pour pouvoir modifier, créer ou supprimer des musiques.
Il y a les deux boutons d'inscription et de connexion à droite du menu.
    Logs admin :
        - Nom d'utilisateur : admin
        - Mot de passe : admin

    Logs user :
        - Nom d'utilisateur : user
        - Mot de passe : user

Il faut etre connecté en admin pour voir le bouton Utilisateur qui amène au menu des utilisateurs. On peut ensuite modifier, créer ou supprimer des utilisateurs.
On a fait le choix d'empécher la création d'utilisateur directement sur le site pour éviter tout problème lié à la sécurité.