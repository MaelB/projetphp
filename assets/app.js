/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

//js
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Home from './js/components/home';
let isLoggedIn = document.getElementById("user_logged")? document.getElementById("user_logged").value : '';
let userName = document.getElementById("user_name")? document.getElementById("user_name").value : '';
let userAdmin = document.getElementById("user_admin")? true : false;
window.USER_DETAILS = {
    user_isLoggedIn: isLoggedIn,
    user_name : userName,
    user_admin: userAdmin
};

ReactDOM.render(<Router><Home user_isLoggedIn={isLoggedIn} user_name={userName} user_admin={userAdmin} /></Router>, document.getElementById('root'));
