import React from 'react';
import {Link} from "react-router-dom";
import Button from "reactstrap/es/Button";

class HomePage extends React.Component {
    render() {
        return (
         <div>
            <div id="carouselExampleControls" className="carousel slide" data-ride="false">
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img className="d-block w-100 " src="https://cdn.caucus.fr/wp-content/uploads/2015/08/2956862e14290964afa37f521f43ac74_large.jpeg" alt="First slide"/>
                    </div>
                    <div className="carousel-item">
                        <img className="d-block w-100 " src="https://statics-zepass.digitick.com/profils/zepass/images/blocs/concert-2.jpg" alt="Second slide"/>
                    </div>
                    <div className="carousel-item" id="img-slide">
                        <img className="d-block w-100" src="https://www.thebalancecareers.com/thmb/gM8FwXq7ro7b4wjZikQPBfMkPio=/4280x2326/filters:fill(auto,1)/young-male-singer-with-a-microphone-performing-on-stage-in-backlit-938763258-5af50467fa6bcc00360fc548.jpg" alt="Third slide"/>
                    </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
             <div className="jumbotron jumbotron-fluid">
                 <div className="container">
                     <h1 className="display-4">Bienvenue sur notre site de musiques!</h1>
                     <p className="lead mt-5">Pour consulter nos musiques, c'est par ici:        <Button
                         className="button-create btn-music ml-4"
                         tag={Link}
                         to={"/musics"}
                         color="primary"
                         size="1x"
                     >
                         Nos musiques
                     </Button> </p>
                 </div>
             </div>
         </div>
        )
    }
}

export default HomePage;
