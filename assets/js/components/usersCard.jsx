import React from 'react';
import { Link } from 'react-router-dom';
import {Button} from "reactstrap";
import img from '../../images/utilisateurs.png';

export class UsersCard extends React.Component{
    constructor(props) {
        super(props);
    }

    async deleteUser (user,e) {
        const requestOptions = {
            method: 'DELETE'
        };
        await fetch(`/api/users/${user.id}`,requestOptions)
            .then(response => {
                if (response.ok) {
                    alert("Suppression réussi");
                    window.location.replace('/users');
                } else {
                    alert("Problème de connexion ");
                }
            })
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
    }

    render () {
        const { user } = this.props;
        const linkToDeviceModification = `/users/${user.id}`
        return (
            <div className="card mt-4">
                <div className="row g-0">
                    <div className="col-md-3 d-flex flex-wrap align-items-center">
                        <img className="logo-music"  alt="50x50" src={img} data-holder-rendered="true"/>
                    </div>
                    <div className="col-md-9">
                        <div className="card-body">
                            <div>
                                <h2 className="card-title">Nom: {user.name}</h2>
                            </div>
                            <div>
                                <h5>Rôles: {user.roles.map(e => <span className="badge badge-pill badge-info ml-2 mt-5" key={e}> {e} </span>)} </h5>
                            </div>
                            <div className="edit-button">
                                <Link user={user} to={linkToDeviceModification}>
                                    <i className="fas fa-edit text-primary "></i>
                                </Link>
                            </div>
                            <div className="delete-button" onClick={(e) => this.deleteUser(user,e)}>
                                <i className="fas fa-trash-alt"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UsersCard;
