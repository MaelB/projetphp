import React from 'react';
import { Container, Row, Col, Button, FormGroup, Label } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import {Field, Form, Formik} from "formik";

export class UsersForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            user: [],
            isNew: true,
            isAdmin: false
        }
    }

    async getUser () {
        await fetch('/api/users/'+this.props.match.params.userId)
            .then(response => {
                if (response.ok) {

                } else {
                    alert("Problème de connexion ");
                }
                return response.json()
            })
            .then(data => {
                this.setState({ user: data});
                this.setState({ isAdmin: this.state.user.roles.includes('ROLE_ADMIN') });
            })
    }

    componentDidMount () {
        if (this.props.match.params.userId !== 'new') {
            this.setState({ isNew: false });
            this.getUser();
        }
    }

    async handleSubmit (values) {
        const body = {
            "name": values.name,
            "password": values.password
        }
        if  (this.state.isNew) {
            const requestOptions = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body)
            };
            fetch('/api/users', requestOptions)
                .then(response => {
                    if (response.ok) {
                        window.location.replace('/users');
                    } else {
                        alert("Problème de connexion ");
                    }
                    return response.json()
                })
                .catch(function (error) {
                    alert("Problème de connexion " + error.message);
                });
        }
        else {
            const requestOptions = {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body)
            };
            fetch('/api/users/'+this.props.match.params.userId, requestOptions)
                .then(response => {
                    if (response.ok) {
                        window.location.replace('/users');
                    } else {
                        alert("Problème de connexion ");
                    }
                    return response.json()
                })
                .catch(function (error) {
                    alert("Problème de connexion " + error.message);
                });
        }

    }

    render () {
        return (
            <>
                { window.USER_DETAILS.user_admin ? (
                    <Container>
                        <div className="conteneur-white p-5 mt-5">
                            <Formik
                                enableReinitialize={true}
                                initialValues={{
                                    name: (this.state.user && this.state.user.name) || '',
                                    password: ''
                                }}
                                onSubmit={async values => this.handleSubmit(values)}
                            >
                                <Form>
                                    <section className="card-form">
                                        <Row className="card-form-header">
                                            <Button
                                                className="bg-white border-0"
                                                tag={Link}
                                                to="/users"
                                            >
                                                <i className="fas fa-angle-left fa-2x" style={{color: 'black'}}></i>
                                            </Button>
                                            <h3 className="title is-4 is-styled">
                                                Propriétées de l'utilisateur
                                            </h3>
                                        </Row>
                                        <Row className="my-3">
                                            <Col xs="12" md="6">
                                                <FormGroup>
                                                    <Label className="input-label">
                                                        Nom
                                                    </Label>
                                                    <Field name="name" type="text" className="form-control" />
                                                </FormGroup>
                                            </Col>
                                            <Col xs="12" md="6">
                                                <FormGroup>
                                                    <Label className="input-label">
                                                        Mot de passe
                                                    </Label>
                                                    <Field name="password" type="password" className="form-control" />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </section>
                                    <Button color="primary" type="submit">
                                        Valider
                                    </Button>
                                </Form>
                            </Formik>
                        </div>
                    </Container>
                ) : (
                    <Redirect to={"/"}/>
                )}
            </>
        )

    }
}

export default UsersForm;
