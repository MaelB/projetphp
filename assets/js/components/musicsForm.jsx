import React from 'react';
import { Container, Row, Col, Button, FormGroup, Label } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import {Field, Form, Formik} from "formik";
import dateFormat from 'dateformat';

export class MusicsForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            music: [],
            isNew: true
        }
    }

    async getMusic () {
        await fetch('/api/musics/'+this.props.match.params.musicId)
            .then(response => {
                if (response.ok) {

                } else {
                    alert("Problème de connexion ");
                }
                return response.json()
            })
            .then(data => {
                data.date = dateFormat(data.date,"yyyy-mm-dd")
                this.setState({ music: data});
            })
    }

    componentDidMount () {
        if (this.props.match.params.musicId !== 'new') {
            this.setState({ isNew: false });
            this.getMusic();
        }
    }

    async handleSubmit (values) {
        if  (this.state.isNew) {
            const requestOptions = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(values)
            };
            fetch('/api/musics', requestOptions)
                .then(response => {
                    if (response.ok) {
                        window.location.replace('/musics');
                    } else {
                        alert("Problème de connexion ");
                    }
                    return response.json()
                })
                .catch(function (error) {
                    alert("Problème de connexion " + error.message);
                });
        }
        else {
            const requestOptions = {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(values)
            };
            fetch('/api/musics/'+this.props.match.params.musicId, requestOptions)
                .then(response => {
                    if (response.ok) {
                        window.location.replace('/musics');
                    } else {
                        alert("Problème de connexion ");
                    }
                    return response.json()
                })
                .catch(function (error) {
                    alert("Problème de connexion " + error.message);
                });
        }

    }

    render () {
        return (
            <>
                { window.USER_DETAILS.user_isLoggedIn ? (
                    <Container>
                        <div className="conteneur-white p-5 mt-5">
                        <Formik
                            enableReinitialize={true}
                            initialValues={{
                                name: (this.state.music && this.state.music.name) || '',
                                author: (this.state.music && this.state.music.author) || '',
                                genre: (this.state.music && this.state.music.genre) || '',
                                album: (this.state.music && this.state.music.album) || '',
                                date: (this.state.music && this.state.music.date) || '',
                            }}
                            onSubmit={async values => this.handleSubmit(values)}
                        >
                            <Form>
                                <section className="card-form">
                                    <Row className="card-form-header">
                                        <Button
                                            className="bg-white border-0"
                                            tag={Link}
                                            to="/musics"
                                        >
                                            <i className="fas fa-angle-left fa-2x" style={{color: 'black'}}></i>
                                        </Button>
                                        <h3 className="title is-4 is-styled">
                                            Propriétées de la musique
                                        </h3>
                                    </Row>
                                    <Row className="my-3">
                                        <Col xs="12" md="6">
                                            <FormGroup>
                                                <Label for="createDanger-name" className="input-label">
                                                    Nom
                                                </Label>
                                                <Field name="name" type="text" className="form-control" />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="6">
                                            <FormGroup>
                                                <Label for="createDanger-name" className="input-label">
                                                    Auteur
                                                </Label>
                                                <Field name="author" type="text" className="form-control" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row className="my-3">
                                        <Col xs="12" md="4">
                                            <FormGroup>
                                                <Label for="createDanger-name" className="input-label">
                                                    Genre
                                                </Label>
                                                <Field name="genre" type="text" className="form-control" />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="4">
                                            <FormGroup>
                                                <Label for="createDanger-name" className="input-label">
                                                    Album
                                                </Label>
                                                <Field name="album" type="text" className="form-control" />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" md="4">
                                            <FormGroup>
                                                <Label for="createDanger-name" className="input-label">
                                                    Date
                                                </Label>
                                                <Field name="date" type="date" className="form-control" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </section>
                                <Button color="primary" type="submit">
                                    Valider
                                </Button>
                            </Form>
                        </Formik>
                        </div>
                    </Container>
                ) : (
                    <Redirect to={"/"}/>
                )}
            </>
        )

    }
}

export default MusicsForm;
