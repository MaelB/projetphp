import React, {Component} from 'react';
import {Route, Switch, Link, Redirect} from 'react-router-dom';
import Musics from "./musics";
import Login from "../login/login";
import {MusicsForm} from "./musicsForm";
import Button from "reactstrap/es/Button";
import Registration from "../login/registration";
import {Users} from "./users";
import {UsersForm} from "./usersForm";
import HomePage from "./homePage";

class Home extends Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <Link className={"navbar-brand"} to={"/"}> Home </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item"> <Link className={"nav-link"} to={"/musics"}> Musiques </Link> </li>
                            { window.USER_DETAILS.user_admin && (
                                <li className="nav-item"> <Link className={"nav-link"} to={"/users"}> Utilisateurs </Link> </li>
                            )}
                        </ul>
                            {window.USER_DETAILS.user_isLoggedIn
                                ?<ul className="navbar-nav ml-auto">
                                    <li className="navbar-text">Connecté en tant que:  {window.USER_DETAILS.user_name} </li>
                                    <li className="nav-item"><Button className="btn btn-outline-info my-2 ml-3 my-sm-0 logout-btn" href='/logout' type="button">Logout</Button></li>
                                </ul>
                                : <ul className="navbar-nav ml-auto">
                                    <li className="nav-item"> <Link className={"nav-link btnregister"} to={"/registration"}> Inscription </Link> </li>
                                    <li className="nav-item"> <Link className={"nav-link"} to={"/login"} > Connexion </Link> </li>
                                </ul>
                            }

                    </div>
                </nav>
                <Switch>
                    <Route path="/users/:userId" component={UsersForm}/>
                    <Route path="/users" component={Users}/>
                    <Route path="/musics/:musicId" component={MusicsForm}/>
                    <Route path="/musics" component={Musics}/>
                    <Route path="/registration" component={Registration}/>
                    <Route path="/login"  component={Login}/>
                    <Route exact path="/" component={HomePage}/>
                </Switch>
            </div>
        )
    }
}

export default Home;
