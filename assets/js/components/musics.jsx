import React from 'react';
import { Container, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import MusicsCard from './musicsCard';

export class Musics extends React.Component{
    _isMounted = false;
    state = {
        musics: [],
        isLoading: false,
        next: null,
        previous: null,
        last: null,
        items: [],
        url: '/api/musics',
        id: null,
        currentItem: null,
        searchOptions: [],
        searchSelectOption: '',
        searchTextOption: ''
    }

    getMusics = async (url) => {
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'},
        };
        this.setState({ isLoading: true });
        await fetch(url, requestOptions)
            .then(response => {
                if (response.ok) {

                } else {
                    alert("Problème de connexion ");
                }
                return response.json()
            })
            .then(data => {
                if (this._isMounted) {
                this.setState({ musics: data['hydra:member']});
                this.setState({ last: data['hydra:view']['hydra:last']});
                this.setState({ id: data['hydra:view']['@id']});
                this.setState({ searchOptions: data['hydra:search']['hydra:mapping']});
                this.setState({ searchSelectOption: this.state.searchOptions[0].property});
                this.setState({ currentItem: this.state.id.substring(this.state.id.indexOf("=") + 1)});
                if (data['hydra:view'] && data['hydra:view']['hydra:next']) {
                    this.setState({next : data['hydra:view']['hydra:next']})
                } else {
                    this.setState({next : null})
                }
                if (data['hydra:view'] && data['hydra:view']['hydra:previous']) {
                    this.setState({previous : data['hydra:view']['hydra:previous']})
                } else {
                    this.setState({previous : null})
                }
                this.nbItems()
            }})
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
        this.setState({ isLoading: false });
    }

    nextPage =  () => {
        this.getMusics(this.state.next)
    }

    previouPage =  () =>  {
        this.getMusics(this.state.previous)
    }

    nbItems =  () =>  {
        let resultHtml = []
        if(this.state.last){
            const lastIem = this.state.last.substring(this.state.last.indexOf("=") + 1)

            for (let i = 0; i < lastIem ; i++){
                resultHtml.push((i+1))
            }
        }
        this.setState({items : resultHtml})
    }

    getPages (pageNum) {
        this.getMusics('/api/musics' + '?page=' + pageNum);
    }

    handleChangeSelect(value) {
        this.setState({ searchSelectOption: value })
    }

    handleChangeText(e) {
        this.setState({ searchTextOption: e })
    }

    searchMusic =  () =>   {
        this.getMusics( '/api/musics'+ '?' + this.state.searchSelectOption + '=' + this.state.searchTextOption);
    }

    componentDidMount () {
        this._isMounted = true;
        this.getMusics('/api/musics');
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.setState = (state,callback)=>{
            return;
        };
    }

    render () {

        return (
            <Container>
                <h1 className="display-1">Nos musiques</h1>
                <div className="conteneur p-5 mt-4">
                    <div className="d-flex justify-content-between flex-wrap">
                        { window.USER_DETAILS.user_isLoggedIn && (
                            <Button
                                className="button-create mb-2"
                                tag={Link}
                                to="/musics/new"
                                color="primary"
                                size="1x"
                            >
                                Creer nouvelle musique
                            </Button>
                        )}
                    {/* Formulaire de recherche */}
                        <div className="form-inline">
                            <input type="text" className="form-control mb-2 mr-sm-2" id="inlineFormInputName2"  value={this.state.searchTextOption} onChange={val => this.handleChangeText(val.target.value)} placeholder="Recherche"/>
                            <div className="input-group mb-2 mr-sm-2">
                             <select id="inputState" className="form-control" value={this.state.searchSelectOption} onChange={val => this.handleChangeSelect(val.target.value)}>
                                    {this.state.searchOptions.map((e ,index) => <option key={index} value={e.property}>{e.property}</option>)}
                             </select>
                            </div>
                            <Button className="btn btn-primary  mb-2" onClick={this.searchMusic}>Rechercher</Button>
                        </div>
                    </div>

                    {/* Affichage des musiques  */}
                    {this.state.isLoading ? (
                        <div className="spinner-border text-info justify-content-center" role="status">
                        </div>
                    ) : (
                        <div>
                            {this.state.musics.map(e=><MusicsCard key={e.id} music={e}/>)}
                        <div>
                            <nav className="mt-5">
                                <ul className="pagination justify-content-end">
                                    <li  className={'page-item ' + (this.state.previous === null ? 'disabled' : '') }>
                                        <Button className="page-link" onClick={this.previouPage}>
                                            Previous
                                        </Button>
                                    </li>
                                    <>{this.state.items.map(e => <li key={e} className='page-item'><Button key={e} active={this.state.currentItem == e} className='page-link' onClick={() => this.getPages(e)} dangerouslySetInnerHTML={{__html: e}}></Button></li>)}</>
                                    <li className={'page-item ' + (this.state.next === null ? 'disabled' : '') }>
                                        <Button className="page-link" onClick={this.nextPage}>
                                            Next
                                        </Button>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        </div>
                    )}
                </div>
            </Container>
        )

    }
}

export default Musics;
