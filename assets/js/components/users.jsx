import React from 'react';
import { Container, Button } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import UsersCard from './usersCard';

export class Users extends React.Component{
    state = {
        users: [],
        isLoading: false
    }

    getUsers = async () => {
        this.setState({ isLoading: true });
        const requestOptions = {
            method: 'GET',
            headers: {'accept': 'application/json'}
        };
        await fetch('/api/users',requestOptions)
            .then(response => {
                if (response.ok) {
                } else {
                    alert("Problème de connexion ");
                }
                return response.json()
            })
            .then(data => {
                this.setState({ users: data });
            })
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
        this.setState({ isLoading: false });
    }

    componentDidMount () {
        this.getUsers();
    }

    render () {

        return (
            <>
                { window.USER_DETAILS.user_admin ? (
                <Container>
                    <h1 className="display-1">Listes des utilisateurs</h1>
                    <div className="conteneur p-5 mt-4">
                        <div className="d-flex justify-content-between flex-wrap">
                            <Button
                                className="button-create mb-2"
                                tag={Link}
                                to="/users/new"
                                color="primary"
                                size="1x"
                            >
                                Creer un nouvel utilisateur
                            </Button>
                        </div>
                        {this.state.isLoading ? (
                            <div className="spinner-border text-info justify-content-center" role="status">
                            </div>
                        ) : (
                            <div>
                                {this.state.users.map(e=><UsersCard key={e.id} user={e}/>)}
                            </div>
                        )}
                    </div>
                </Container>
                ) : (
                    <Redirect to={"/"}/>
                )}
            </>
        )

    }
}

export default Users;
