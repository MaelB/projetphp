import React from 'react';
import { Link } from 'react-router-dom';
import img from '../../images/musicicLogo.png';

export class MusicsCard extends React.Component{
    constructor(props) {
        super(props);
    }

    async deleteMusic (music,e) {
        const requestOptions = {
            method: 'DELETE'
        };
        await fetch(`/api/musics/${music.id}`,requestOptions)
            .then(response => {
                if (response.ok) {
                    alert("Suppression réussi");
                    window.location.replace('/musics');
                } else {
                    alert("Problème de connexion ");
                }
            })
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
    }

    render () {
        const { music } = this.props;
        const linkToDeviceModification = `/musics/${music.id}`
        return (
            <div className="card mt-4">
                <div className="row g-0">
                    <div className="col-md-3 d-flex flex-wrap align-items-center">
                        <img className="logo-music"  alt="50x50" src={img} data-holder-rendered="true"/>
                    </div>
                    <div className="col-md-9">
                        <div className="card-body">
                            <h2 className="card-title">{music.name} </h2>
                            <h5 className="card-text">Album: {music.album}</h5>
                            <footer>
                            <div className="footer-card d-flex">Genre: {music.genre}</div>
                            <div className="d-flex justify-content-between">
                                <div>Autheur: {music.author}</div>
                                <div className="footer-card">Date: {music.date.substring(0, music.date.indexOf('T'))}</div>
                            </div>
                            </footer>
                            { window.USER_DETAILS.user_isLoggedIn && (
                                <>
                                    <div className="edit-button">
                                        <Link music={music} to={linkToDeviceModification}>
                                            <i className="fas fa-edit text-primary "></i>
                                        </Link>
                                    </div>
                                    <div className="delete-button" onClick={(e) => this.deleteMusic(music,e)}>
                                        <i className="fas fa-trash-alt"></i>
                                    </div>
                                </>
                            )}

                        </div>
                    </div>
            </div>
            </div>
        )
    }
}

export default MusicsCard;
