import React from 'react';
import {Button, Container} from 'reactstrap';
import {Field, Form, Formik} from "formik";

export class Registration extends React.Component{
    _isMounted = false;
    state = {
        errors: false,
        isDisable: false,
        pseudoUsed: false,
        userCreated: false,
        pseudos: [],
        actualPseudo: ""
    }

    componentDidMount = async () => {
        this._isMounted = true;
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'},
        };
        await fetch('/api/users', requestOptions)
            .then(response => {
                if (response.ok) {

                } else {
                    alert("Problème de connexion ");
                }
                return response.json()
            })
            .then(data => {
                if (this._isMounted) {
                    this.setState({pseudos: data['hydra:member']});
                }
            })
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.setState = (state,callback)=>{
            return;
        };
    }

    async handleSubmit (values) {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(values)
        };

        fetch('/api/users', requestOptions)
            .then(response => {
                if (response.ok) {
                    this.setState({userCreated: true});
                } else {
                    this.setState({ errors: true })
                }
                return response.json()
            })
            .catch(function (error) {
                alert("Problème de connexion " + error.message);
            });
    }

    handleChangePseudo(pseudos) {
        let error;
        for (const p of this.state.pseudos.map(e=> e.name)) {
            if (p === pseudos) {
                this.setState({ pseudoUsed: true })
                this.setState({ isDisable: true })
                return error;
            }
        }
        this.setState({ pseudoUsed: false })
        this.setState({ isDisable: false })
        return
    }


    render (){
        return(
            <Container>
                <div className="conteneur-white p-5 mt-5">
                    <h1>Inscription</h1>

                    <Formik
                        validateOnChange
                        initialValues={{name: "", password: ""}}
                        onSubmit={async values => this.handleSubmit(values)}
                    >
                        <Form>
                            {this.state.errors === true &&
                            <div className="alert alert-danger" role="alert">
                                Il y a eu un problème lors de la création de l'utilisateur
                            </div>
                            }
                            {this.state.pseudoUsed &&
                            <div className="alert alert-danger">Pseudo déjà utilisé</div>
                            }
                            {this.state.userCreated &&
                            <div className="alert alert-success">L'utilisateur a été créé</div>
                            }
                            <div className="form-group">
                                <label htmlFor="name" >Nom d'utilisateur</label>
                                <Field type="text" name="name" id="name" className="form-control" onBlur={val => this.handleChangePseudo(val.target.value)} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" >Mot de passe</label>
                                <Field  type="password" name="password"  id="password" className="form-control" required/>
                            </div>

                            <Button type="submit" disabled={this.state.isDisable} className="btnregister">S'inscrire</Button>
                        </Form>
                    </Formik>
                </div>
            </Container>
        )
    }
}
export default Registration;
