import React from 'react';
import {Button, Container} from 'reactstrap';
import { Link } from 'react-router-dom';
import {Field, Form, Formik} from "formik";

export class Login extends React.Component{
	state = {
		errors: false,
		isDisable: false
	}

	async handleSubmit (values) {
		const requestOptions = {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify(values)
		};

		fetch('/api/login', requestOptions)
			.then(response => {
				if (response.ok) {
					window.location.replace('/');
				} else {
					this.setState({ errors: true })
				}
				return response.json()
			})
			.catch(function (error) {
				alert("Problème de connexion " + error.message);
			});
	}


	render () {

		return (
			<Container>
				<div className="conteneur-white p-5 mt-5">
				<h1>Connexion</h1>
				<Formik
					initialValues={{username: "", password: ""}}
					onSubmit={async values => this.handleSubmit(values)}
				>
					<Form
						className="needs-validation"
					>
						{this.state.errors === true &&
						<div className="alert alert-danger" role="alert">
							Problème de connexion, mot de passe ou login incorrect
						</div>
						}
						<div className="form-group">
							<label htmlFor="name">Nom d'utilisateur</label>
							<Field type="text" name="username"  id="username" className="form-control" required/>
							<div className="invalid-feedback">
								Merci d'indiquer votre nom d'utilisateur
							</div>
						</div>
						<div className="form-group">
							<label htmlFor="password" >Mot de passe</label>
							<Field  type="password" name="password" id="password" className="form-control" required/>
							<div className="invalid-feedback">
								Merci d'indiquer votre mot de passe
							</div>
						</div>

						<Button type="submit" disabled={this.isDisable} className="btn btn-primary">Se connecter</Button>
					</Form>
				</Formik>
				<Link to="/registration">Vous n'avez pas encore de compte? Inscrivez vous.</Link>
				</div>
			</Container>
		)

	}
}
export default Login;
